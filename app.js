
// // Requiring module
// const express = require('express');
 
// // Creating express object
// const app = express();
 
// // Handling GET request
// app.get('/', (req, res) => { 
//     res.send('A simple Node App is '
//         + 'running on this server') 
//     res.end() 
// }) 
 
// // Port Number
// const PORT = process.env.PORT ||5000;
 
// // Server Setup
// app.listen(PORT,console.log(
//   `Server started on port ${PORT}`));
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

// Middleware to parse incoming request bodies
app.use(bodyParser.urlencoded({ extended: false }));

// Serve HTML form
app.get('/', (req, res) => {
    res.send(`
        <form action="/submit" method="post">
            <label for="name">Name:</label>
            <input type="text" id="name" name="name"><br><br>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email"><br><br>
            <button type="submit">Submit</button>
        </form>
    `);
});

// Handle form submission
app.post('/submit', (req, res) => {
    const name = req.body.name;
    const email = req.body.email;
    
    // Do something with the form data, like saving to a database
    console.log(`Received form submission: Name - ${name}, Email - ${email}`);
    
    res.send(`Form submitted successfully. Name: ${name}, Email: ${email}`);
});

// Start the server
app.listen(port, () => {
    console.log(`Server is listening at http://localhost:${port}`);
});
